from visual import emojies


def create():
    field = [[0] * 10 for x in range(10)]
    return field


def hide(pole):
    index = 0
    visual = []
    visual.append(' '.join(emojies.numbers))
    for line in pole:
        for i in range(10):
            if line[i] in [0, 1, 2, 3, 4, 'z']:
                line[i] = emojies.zero
        visual.append(' '.join([str(x) for x in line]) + emojies.alph[index])  # list of lines with space
        index += 1
    return '\n'.join(visual)


def show(pole):
    visual = []
    for line in pole:
        visual.append(' '.join([str(x) for x in line]))  # list of lines with space
    return '\n'.join(visual)


def showz(pole):
    index = 0
    visual = []
    visual.append(' '.join(emojies.numbers))
    for line in pole:
        for i in range(10):
            if line[i] in ['z', 0]:
                line[i] = emojies.zero
            elif line[i] in [1, 2, 3, 4]:
                line[i] = emojies.ship
        visual.append(' '.join([str(x) for x in line]) + emojies.alph[index])  # list of lines with space
        index += 1
    return '\n'.join(visual)
