import json
import os


def add_player(message):
    path = 'game/players.json'

    if not os.path.isfile(path):
        empty_list = []
        with open(path, "w+", encoding="utf-8") as file:
            json.dump(empty_list, file)

    with open(path, "r", encoding="utf-8") as file:
        players = json.load(file)
    if '@' + str(message.chat.username) in players:
        return
    else:
        players.append('@' + str(message.chat.username))
        os.remove(path)
        with open(path, "w+", encoding="utf-8") as file:
            json.dump(players, file)


def get_hash(player):
    path = 'game/' + str(player) + '.json'
    with open(path, "r", encoding="utf-8") as file:
        rn = json.load(file)
    hash = sum([int(x) for x in rn[0].split()])
    return (hash)


def save_files(files):
    hash = get_hash(files[0][0])
    path = 'game/' + str(hash) + '_files.json'
    os.remove(path)
    with open(path, "w+", encoding="utf-8") as file:
        json.dump(files, file)


def load_files(player):
    hash = get_hash(player)
    path = 'game/' + str(hash) + '_files.json'
    with open(path, "r", encoding="utf-8") as file:
        files = json.load(file)

    return (files)


def save_players(players):
    a = []
    for_save = ' '.join([str(x) for x in players])
    for i in players:
        path = 'game/' + str(i) + '.json'

        if not os.path.isfile('game/' + str(i) + '.json'):
            with open(path, "w", encoding="utf-8") as file:
                json.dump(a, file)

        with open(path, "r", encoding="utf-8") as file:
            rn = json.load(file)
        os.remove(path)

        with open(path, "w+", encoding="utf-8") as file:
            rn.append(' '.join([str(x) for x in players]))
            json.dump(rn, file)


def create_files(players, fields, enoughs, turn):  # [[player1, player2], [fields], [enoughs], [turn],
    # [dict_id], [endgins], [messages], [old turn]
    files = []
    files.append(players)
    files.append(fields)
    files.append(enoughs)
    files.append(turn)
    files.append({str(players[0]): 0, str(players[1]): 1})
    files.append([0, 0])  # endings
    files.append([[0, 0], [0, 0], [0, 0]])  # messages for edit, turns
    files.append(0)  # old turn
    print(len(files))
    print(files)

    name = str(players[0] + players[1])
    with open('game/' + name + "_files.json", "w", encoding="utf-8") as file:
        json.dump(files, file)

    save_players(players)


def turn_reverse(turn):
    if turn:
        turn = 0
    else:
        turn = 1
    return turn
