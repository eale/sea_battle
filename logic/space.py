from visual import field
from logic import place_ships


def get_info():
    cord = input()
    size = int(input())
    orient = input()  # vertical or horizontal
    return (size, cord, orient)


def make_choice(pole, message):
    try:
        cord, size, orient = message.text.split()
        pole = place_ships.place_ship(pole, int(size), cord, orient, message)
        if pole:
            return pole
        return False
    except:
        return False


def space(pole, message):
    new_pole = make_choice(pole, message)
    if new_pole:
        return new_pole


if __name__ == '__main__':
    main(field.create())
