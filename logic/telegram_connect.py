import telebot
from telebot import types
from copy import deepcopy
from logic import space
from logic import place_ships
from logic import hit
from logic import functions
from visual import field
from visual import emojies
import config
import os
import time

fields = [field.create(), field.create()]
players = []
enoughs = [[1, 1, 1, 1, 2, 2, 2, 3, 3, 4], [1, 1, 1, 1, 2, 2, 2, 3, 3, 4]]
turn = 0

bot = telebot.TeleBot(config.token)


def edit_message_2(files, turn, players):
    first = functions.turn_reverse(turn)

    pole = files[1][turn]

    print("BOOOOOOM")
    print(turn)
    print(files[6][first][1])
    print(players[first])
    time.sleep(0.5)
    try:
        bot.edit_message_text(chat_id=players[first],
                              message_id=files[6][2][first], text=emojies.phrases['you_turn'])
    except:
        pass
    try:
        bot.edit_message_text(chat_id=players[turn],
                              message_id=files[6][2][turn], text=emojies.phrases['enemy_turn'])
    except:
        pass

    try:
        bot.edit_message_text(chat_id=players[first],
                              message_id=files[6][first][1], text=field.hide(deepcopy(pole)))
    except:
        pass

    try:
        bot.edit_message_text(chat_id=players[turn],
                              message_id=files[6][turn][1], text=field.showz(deepcopy(pole)))
    except:
        pass


def edit_message(files, turn, players):
    error = 0
    if files[7] != turn:
        turn = files[7]
        error = 1

    old_pole = files[1][files[7]]
    first = functions.turn_reverse(turn)
    message_1_chat_id, message_1_message_id = files[6][first][0], files[6][first][1]
    message_2_chat_id, message_2_message_id = files[6][turn][0], files[6][turn][1]
    print(message_1_message_id, message_1_chat_id)

    bot.edit_message_text(chat_id=message_1_chat_id,
                          message_id=message_1_message_id, text=field.hide(deepcopy(old_pole)))
    bot.edit_message_text(chat_id=message_2_chat_id,
                          message_id=message_2_message_id, text=field.showz(deepcopy(old_pole)))

    time.sleep(1)
    if error == 1:
        turn = functions.turn_reverse(turn)
    edit_message_2(files, turn, players)


def callback_inline(call):
    files = functions.load_files(call.message.chat.id)
    endings = files[5]
    print(files)
    players = files[0]
    id = files[4][str(call.message.chat.id)]
    if call.data == "test":
        place_ships.random_ships(call.message, files)
        bot.send_message(call.message.chat.id, field.showz(deepcopy(files[1][id])))
        endings[files[4][str(call.message.chat.id)]] = 1
        print(files)
        functions.save_files(files)
        print(endings)
        if sum(endings) == 2:
            bot.send_message(players[0], emojies.phrases['game_started'])
            bot.send_message(players[1], emojies.phrases['game_started'])

            first_show(files[0], functions.turn_reverse(turn), files)

            files[5] = 'end'
            functions.save_files(files)


def hit_round(message):
    files = functions.load_files(message.chat.id)
    turn = files[3]
    enough = files[2][functions.turn_reverse(turn)]
    players = files[0]
    try:
        endings = files[5]
    except:
        return

    if endings != 'end':
        return

    if message.chat.id != players[turn]:
        return

    boolean = hit.hit(message.text, message, players, functions.turn_reverse(turn), files)
    if boolean in list(emojies.phrases):
        bot.send_message(players[turn], emojies.phrases[boolean])  # errors
        first_show(players, functions.turn_reverse(files[3]), files)
        return False

    if not enough:
        first_show(players, functions.turn_reverse(turn), files)
        bot.send_message(message.chat.id, emojies.phrases['win'])
        bot.send_message(players[functions.turn_reverse(turn)], emojies.phrases['loss'])
        hash = functions.get_hash(message.chat.id)
        os.remove('game/' + str(hash) + '_files.json')
        os.remove('game/' + str(message.chat.id) + '.json')
        os.remove('game/' + str(players[functions.turn_reverse(turn)]) + '.json')
        return

    if not boolean:
        print('hi')
        files[3] = functions.turn_reverse(turn)

    functions.save_files(files)

    first_show(players, functions.turn_reverse(files[3]), files)


def first_show(players, turn, files):
    if files[6][0][0] != 0:
        edit_message(files, turn, players)  # edit previous field
        files[7] = turn
        functions.save_files(files)
        return

    if turn:
        first = 0
    else:
        first = 1
    pole = files[1][turn]
    message_turn_1 = bot.send_message(players[first], emojies.phrases['you_turn'])
    message_1 = bot.send_message(players[first], field.hide(deepcopy(pole)))

    message_turn_2 = bot.send_message(players[turn], emojies.phrases['enemy_turn'])
    message_2 = bot.send_message(players[turn], field.showz(deepcopy(pole)))

    files[6][0][0], files[6][0][1] = message_1.chat.id, message_1.message_id
    files[6][1][0], files[6][1][1] = message_2.chat.id, message_2.message_id
    files[6][2][0], files[6][2][1] = message_turn_1.message_id, message_turn_2.message_id
    files[7] = turn

    functions.save_files(files)


@bot.message_handler(commands=["start"])
def get_start(message):
    functions.add_player(message)
    global players
    if os.path.exists('game/' + str(message.chat.id) + '.json'):
        return
    if message.from_user.id in players and len(players) < 2:
        bot.send_message(message.chat.id, emojies.phrases['spam'])
        return False

    keyboard = types.InlineKeyboardMarkup()
    random_ships = types.InlineKeyboardButton(text=emojies.phrases['random'], callback_data="test")
    keyboard.add(random_ships)

    global players_id
    if message.from_user.id not in players:
        players.sort()
        players.append(message.from_user.id)
        print(players)
        if len(players) == 2:
            bot.send_message(players[0], emojies.phrases['match_found'])
            bot.send_message(players[1], emojies.phrases['match_found'])
            bot.send_message(players[0], field.showz(deepcopy(fields[0])), reply_markup=keyboard)
            bot.send_message(players[1], field.showz(deepcopy(fields[0])), reply_markup=keyboard)
            players_id = {players[0]: 0,
                          players[1]: 1}

            functions.create_files(players, fields, enoughs, turn)  # [[player1, player2], [fields], [enoughs], [turn]]
            players = []

            return True
        bot.send_message(message.chat.id, emojies.phrases['in_the_queue'])
        print(players)
        return False


@bot.callback_query_handler(func=lambda call: True)
def hz(call):
    callback_inline(call)


@bot.message_handler(content_types=["text"])
def get_text(message):
    bot.delete_message(message.chat.id, message.message_id)
    if not os.path.exists('game/' + str(message.chat.id) + '.json'):
        return
    files = functions.load_files(message.chat.id)
    endings = files[5]
    if message.from_user.id in players and len(players) < 2:
        bot.send_message(message.chat.id, emojies.phrases['spam'])
        return False

    # Расстановка кораблей
    def get_pole(message):
        try:
            endings = files[5]
        except:
            return
        print(functions.load_files(message.chat.id))
        keyboard = types.InlineKeyboardMarkup()
        random_ships = types.InlineKeyboardButton(text=emojies.phrases['random'], callback_data="test")
        keyboard.add(random_ships)

        if endings == 'end':
            return False
        print(len(files))
        print(files)
        id = files[4][str(message.chat.id)]
        enough = files[2][id]
        turn = files[3]
        players = files[0]
        if not enough:
            bot.send_message(message.chat.id, emojies.phrases['wait_the_enemy'])
            return False

        try:
            new_pole, enough = space.space(deepcopy(files[1][id]), message)
            files[2][id] = enough
            print('hi', new_pole)
        except:
            new_pole = False

        print(enough)
        if new_pole:
            files[1][id] = new_pole
            functions.save_files(files)
            bot.send_message(message.chat.id, field.showz(deepcopy(new_pole)), reply_markup=keyboard)

            try:
                bot.send_message(message.chat.id, ' '.join([str(x) for x in enough]))
            except:
                endings[id] = 1
                files[2][id] = enoughs[0].copy()
                functions.save_files(files)
                if sum(endings) == 2:
                    functions.save_files(files)
                    bot.send_message(players[0], emojies.phrases['game_started'])
                    bot.send_message(players[1], emojies.phrases['game_started'])

                    first_show(players, functions.turn_reverse(turn), files)

                    endings = 'end'
                    return False
                bot.send_message(message.chat.id, emojies.phrases['enough_end'])
        print(field.show(deepcopy(files[1][id])))

    get_pole(message)

    hit_round(message)

    # Если пользователь нажал на кнопку рандомной расстановки
    @bot.callback_query_handler(func=lambda call: True)
    def hz(call):
        callback_inline(call)


bot.polling(none_stop=True)
