import random
from logic import functions


def clear_massive(pole):
    for i in range(10):
        for j in range(10):
            try:
                pole[i][j] = int(pole[i][j])
            except:
                pass
    return pole


def check_enough(size, enough):
    if size not in enough:
        return False
    return True


def check_and_change(cord):
    try:
        if ord('a') <= ord(cord[0].lower()) <= ord('j') and 1 <= int(''.join(cord[1::])) <= 10:  # check
            cord[0] = ord(cord[0].lower()) - 97
            cord[1] = int(''.join(cord[1::])) - 1  # index start from 1
            return (cord)
        else:
            return False  # Неверный адрес для корабля
    except:
        return False


def add_z(pole, cord1, cord2, index, size, orient):
    for i in range(-1, 2, 1):
        for j in range(-1, 2, 1):
            try:
                if orient == 'horizontal' and i == 0 and j == 1 and not (size == index + 1):
                    continue
                if orient == 'vertical' and i == 1 and j == 0 and not (size == index + 1):
                    continue
                if pole[cord1 + i][cord2 + j] == 0:
                    if cord1 + i >= 0 and cord2 + j >= 0:
                        pole[cord1 + i][cord2 + j] = 'z'
            except:
                pass
    return pole


def check_place(new_pole, size, cord, orient):
    try:
        diff = 0
        for i in range(size):
            if orient == 'horizontal':
                cord1 = cord[0]
                cord2 = cord[1] + diff

            elif orient == 'vertical':
                cord1 = cord[0] + diff
                cord2 = cord[1]
            diff += 1
            if new_pole[cord1][cord2] != 0:
                return False
    except:
        return False
    return True


def place_ship(pole, size, cord, orient, message):
    files = functions.load_files(message.chat.id)
    id = files[4][str(message.chat.id)]
    enough = files[2][id]
    new_pole = pole.copy()
    cord = [x for x in cord]  # str -> list
    cord = check_and_change(cord)

    if not check_enough(size, enough.copy()):
        print("У вас не такого корабля")
        return False
    if not cord:
        print("Неверный адрес корабля")
        return "error#2"  # Неверный адрес корабля(ВЫЗВАТЬ ФУНКЦИЮ ИМПОРТА)

    if not check_place(new_pole, size, cord, orient):  # Проверка места
        print("Место занято или выбрано неудачно")
        return False
    try:
        diff = 0
        for i in range(size):
            if orient == 'horizontal':
                cord1 = cord[0]
                cord2 = cord[1] + diff

            elif orient == 'vertical':
                cord1 = cord[0] + diff
                cord2 = cord[1]

            if new_pole[cord1][cord2] == 'z':
                return False
            new_pole[cord1][cord2] = size
            new_pole = add_z(new_pole, cord1, cord2, i, size, orient)
            diff += 1
    except:
        print("Невозможно установить корабль")
        return False  # Корабль невозможно туда установить(ВЕРНУТЬ ФУНКЦИЮ ИНПУТА)
    print(enough)
    try:
        enough.remove(size)
    except ValueError:
        return False

    # Сохранение
    functions.save_files(files)
    return new_pole, enough


def random_ships(message, files):
    bad_cords = []
    id = files[4][str(message.chat.id)]
    enough = files[2][id]
    pole = files[1][id]
    # print(enough)
    while enough:  # while enough:
        cord = random.choice(['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j']) \
               + random.choice([str(x) for x in range(1, 11)])
        if cord in bad_cords:
            continue

        bad_cords.append(cord)
        size = random.choice(enough)
        orient = random.choice(['vertical', 'horizontal'])

        place = place_ship(pole, size, cord, orient, message)

        if place:
            pole, enough = place
            continue

        elif orient == 'vertical':
            place = place_ship(pole, size, cord, orient, message)
            if place:
                pole, enough = place
                continue

        elif orient == 'horizontal':
            place = place_ship(pole, size, cord, orient, message)
            if place:
                pole, enough = place
                continue
