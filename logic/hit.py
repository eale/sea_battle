from logic import place_ships
from visual import emojies
from visual import field
from copy import deepcopy
from logic import functions


def boom(pole, cord1, cord2):
    global size
    # print(cord1, cord2)
    for i in range(-1, 2, 1):
        for j in range(-1, 2, 1):
            try:
                if cord1 + i >= 0 and cord2 + j >= 0:
                    if pole[cord1 + i][cord2 + j] in ['z', 0]:
                        print('BOOM')
                        pole[cord1 + i][cord2 + j] = emojies.fall

                    elif pole[cord1 + i][cord2 + j] == emojies.fire:
                        size += 1
                        pole[cord1 + i][cord2 + j] = emojies.boom_ship
                        # print(field.showz(pole.copy()))
                        pole = boom(pole.copy(), cord1 + i, cord2 + j)
            except:
                continue
    print(field.showz(deepcopy(pole)))
    return pole


def check_ship(pole, cord1, cord2, checked_cords):
    print('check')
    for i in range(-1, 2, 1):
        for j in range(-1, 2, 1):
            try:
                if cord1 + i >= 0 and cord2 + j >= 0:
                    if pole[cord1 + i][cord2 + j] in [1, 2, 3, 4]:
                        print(pole[cord1 + i][cord2 + j])
                        return True
                    if pole[cord1 + i][cord2 + j] == emojies.fire and (cord1 + i, cord2 + j) not in checked_cords:
                        checked_cords.append((cord1, cord2))
                        if check_ship(deepcopy(pole), cord1 + i, cord2 + j, checked_cords):
                            return True
            except:
                continue
    return False


def hit(cord, message, players, turn, files):
    id = files[4][str(message.chat.id)]
    enough = files[2][functions.turn_reverse(id)]
    checked_cords = []
    global size
    size = 0

    cord = [x for x in cord]
    cord = place_ships.check_and_change(cord)
    print(cord)

    if cord:
        cord1 = cord[0]
        cord2 = cord[1]
        pole = files[1][functions.turn_reverse(id)]
        if pole[cord1][cord2] in [emojies.fire, emojies.fall,
                                  emojies.boom_ship]:
            return "error#1"
        if pole[cord1][cord2] in [1, 2, 3, 4]:

            pole[cord1][cord2] = emojies.fire
            print(field.showz(deepcopy(pole)))

            if not check_ship(deepcopy(pole), cord1, cord2, checked_cords):
                files[1][functions.turn_reverse(id)] = boom(deepcopy(pole), cord1, cord2)
                print('BOOM', pole)
                print(size)
                print(enough)
                enough.remove(size)
            return True
        else:
            pole[cord1][cord2] = emojies.fall
            print(field.showz(deepcopy(pole)))
            return False

    else:
        return 'error#2'
